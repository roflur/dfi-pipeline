rm(list = ls())

library(raster)
library(rgdal)
library(spatialdfi)


options(spam.nearestdistnnz=c(2^31,1000))


# first download data from https://geodata.ucdavis.edu/cmip6/10m/GFDL-ESM4/ssp370/wc2.1_10m_tmax_GFDL-ESM4_ssp370_2081-2100.tif

# load data
name <- "wc2.1_10m_bioc_GFDL-ESM4_ssp370_2081-2100.tif"
bioc <- raster::stack(name)

# plot(bioc@layers[[2]], ylim = c(30, 72), xlim = c(-12, 40))
# 19 layers, described in https://www.worldclim.org/data/bioclim.html

# subset area over Europe
europebioc <- crop(bioc@layers[[2]], extent(-12, 40, 30, 72)) # xmin, xmax, ymin, ymax
# plot(europebioc)


# set spatial locations
locs <- coordinates(europebioc)
y <- t(as.matrix(europebioc)[1:europebioc@nrows,])
originaldim <- dim(y)


# scale data
# image(y)
unscaled_y <- y[!is.na(y)]
complete_y <- c(scale(c(y)))
complete_locs <- locs
complete_n <- length(y)

# rm sea NAs
locs <- locs[!is.na(y),]
y <- c(scale(c(y[!is.na(y)])))
n <- length(y)

# plot(locs, pch = 15, cex = .2, col = fields::tim.colors(n=256)[cut(c(y), breaks = 256)], asp = 1.2)
# hist(y)

# remove North-South trend
yfit <- lm(y ~ locs[,2])
y_detrended <- resid(yfit)
nstrend <- fitted(yfit)

# hist(y_detrended)
# plot(locs, pch = 15, cex = .2, col = fields::tim.colors(n=256)[cut(c(nstrend), breaks = 256)], asp = 1.2)
