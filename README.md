This gitlab repository contains the R-code which is introduced in "Flury R. and Furrer R. (2022). Pipeline to identify dominant features in large spatial data".

### how to install devel packages
  The directory source contains the version of the spatialdfi devel R package which was used for this analysis.

  It can be installed in R with the function, e.g. `install.packages("source/spatialdfi_0.1.tar.gz", repos=NULL)`.

 
### R-Code

 The templates directory contains the R scripts to identify dominant features in synthetic example, each assuming different spatial data structure:
 
 * 01_createdata.R - code to create synthetic data.
 
 * 01a_regularlattice.R - code for complete regular lattice data.
 
 * 01b_regularlattice_ignoreNA.R - code for complete regular lattice data but ignoring specific values.
 
 * 01c_regularlattice_resampleNA.R - code for complete regular lattice data and resample missing values.
 
 * 01d_irregularlattice.R - code for irregular lattice data.
 
 * 01e_georeferenced_matern.R - code for georeferenced data.

 * 01f_georeferenced_wendland.R - code for georeferenced data approximated with compactly supported Wendland covariance function.

 * 01e_georeferenced_nngp.R - code for georeferenced data approximated with nearest neighbor Gaussian process approximateion.

 * 01h_overfitting.R - code for illustrating functions to blockwise subsate spatial data and repeat analysis for each subset.
 

 The illustration directory contains the R scripts to identify dominant features in the illustration section of the associated paper:

 * the data used in the illustration can be downloaded via https://geodata.ucdavis.edu/cmip6/10m/GFDL-ESM4/ssp370/wc2.1_10m_tmax_GFDL-ESM4_ssp370_2081-2100.tif
 
 * 02_loaddata.R - code to load data from tiff image, subset, detrend and standardize the data.
 
 * 02a_wendland.R - code using a wendland approximation of the DFI - (case a).
 
 * 02b_nngp.R - code using a NNGP approximation of the DFI - (case a).
 
 * 02c_discretization.R - code using a IGMRF approximation of the DFI - (case c).



for further questions, contact roman.flury@math.uzh.ch or reinhard.furrer@math.uzh.ch


