
# With the following code two realizations of Gaussian fields are created.
# In each case a different parametrization of the respective Matern covariance function is used.
# The resulting data y is the sum of these fields.

library(spam)

if (!file.exists("spatialdata_example.RData")) {

  # set a seed for reproducible results
  set.seed(111222)

  # define true thetas = [range, psill, smoothness, nugget]
  theta1 <- c(0.05, psill <- 1, 0.5, 0)
  theta2 <- c(0.2,  psill,      2.5, 0)

  # define number of locations n
  n <- 2^10
  dim1 <- dim2 <- sqrt(n)
  locs <- expand.grid(seq(0, 1, length.out = dim1), seq(0, 1, length.out = dim2))

  # calculate the distance matrix between all locations
  distmat <- as.matrix(dist(locs))

  # create field 1
  iidsample1 <- rnorm(n)
  Sigma1 <- spam::cov.finnmat(h = distmat, theta = theta1)
  cholS1 <- chol(Sigma1)
  maternsample1 <- iidsample1 %*% cholS1

  # create field 2
  iidsample2 <- rnorm(n)
  Sigma2 <- spam::cov.finnmat(h = distmat, theta = theta2)
  cholS2 <- chol(Sigma2)
  maternsample2 <- iidsample2 %*% cholS2

  y <- maternsample1 + maternsample2

  # save the following objects in the definied working directory
  save(locs, dim1, dim2, y, theta1, theta2,
       file = "spatialdata_example.RData")

} else {

  load(file = "spatialdata_example.RData")

}


# visualization of the two individual fields and their sum y
if (FALSE) {

  par(mfrow = c(1, 3), mar = c(.4, .4, .4, .4))
  this_colors <- fields::tim.colors(n = 256)[cut(maternsample1, 256)]
  plot(locs, col = this_colors, pch = 20, cex = 3,
       yaxt = "n", xaxt = "n", ylab = "", xlab = "")
  this_colors <- fields::tim.colors(n = 256)[cut(maternsample2, 256)]
  plot(locs, col = this_colors, pch = 20, cex = 3,
       yaxt = "n", xaxt = "n", ylab = "", xlab = "")
  this_colors <- fields::tim.colors(n = 256)[cut(y, 256)]
  plot(locs, col = this_colors, pch = 20, cex = 3,
       yaxt = "n", xaxt = "n", ylab = "", xlab = "")
}

# Roman Flury and Reinhard Furrer 2022
