rm(list = ls())

# spatial feature identification of regular lattice data with areas which are not of interest (NA)

library(spam)
library(fields)
library(spatialdfi)

library(gstat)
library(sp)


# load artificial data y created with 01_createdata.R
load("spatialdata_example.RData")

# preprocessing
y <- matrix(c(scale(c(y))), dim1, dim2)

# set locations to NA and save indeces
yNA <- y
yNA[15:25, 16:24] <- NA
iNA <- which(is.na(c(yNA)))

# create precision matrix
Q <- spam::precmat.IGMRFreglat(dim1, dim2, order = 1)

# spatial data reconstruction
sample <- spatialdfi::rmvnormGibbs.cond(y,
                                   n = 1000,
                                   ignoreNA = TRUE,
                                   iNA = iNA,
                                   verbose = TRUE,
                                   nBurnIn = 10000,
                                   precision = Q,
                                   alpha = c(10, 1),
                                   beta = c(1, 0.1))

# calculate posterior mean
x <- apply(sample$postSample, 2, mean)


# visualization
fields::image.plot(y)
fields::image.plot(yNA)

# visualization of average of posterior samples with their original structure
fields::image.plot(matrix(spatialdfi::setOriginalStructure(data = x, initDim = c(dim1, dim2), iDeleted = iNA),
                          nrow = dim1, ncol = dim2))


# visualizations of updated kappas - traceplot
plot(sample$kappapost[,1], type = "l")
plot(sample$kappapost[,2], type = "l")


# scale selection with scale derivative
lambdas <- 10^seq(-3, 10, len = 1000)
lderiv <- sapply(lambdas, spatialdfi:::logderiv_igmrf, sample = x, precision = Q, ignoreNA = TRUE,
                 iNA = iNA, norm = 1, norm.type = "m")

# visualization of scale derivate and local minima
plot(x = log(lambdas), y = lderiv, type = "l")
minlambda <- lambdas[spatialdfi::localmin(x = lderiv, threshold = 1L)$minima[1]]
abline(v = log(minlambda) )


# scale selection with taperingplots - verification
spatialdfi::TaperingPlot(smoothingLevels = c(1, minlambda), precision = Q, eigenVal = eigen(Q)$values)



details <- spatialdfi::decomposition_igmrf(posteriorSample = t(sample$postSample),
                                           precision = Q,
                                           smoothingLevels = c(1, minlambda, 3000),
                                           dimGrid = c(Q@dimension[1] , 1),
                                           returnSmoothedSamples = FALSE,
                                           ignoreNA = TRUE,
                                           iNA = iNA)



# variogram estimations of the details
getvariogramfit <- function(detail, inwith = 1, incutoff = 70) {

  if (sum(is.na(detail)) != 0) {
    locs <- locs[!is.na(detail), ]
    detail <- c(detail)[!is.na(detail)]
  }
  sp::coordinates(locs) <- ~ Var1 + Var2

  variogram_z1 <-gstat::variogram(c(detail) ~ 1, locations = locs, cressie = T, covariogram = FALSE,
                                  width = inwith, cutoff = incutoff, verbose = TRUE)
  mV <- gstat::vgm(model = c("Mat"))
  (fV_z1 <- gstat::fit.variogram(variogram_z1, mV, fit.kappa = T, fit.sills = T, fit.method = 7))

  return(c(fV_z1$range, fV_z1$psill, fV_z1$kappa))
}

getvariogramfit(detail = c(details$smMean[[1]]), inwith = 1e-4, incutoff = 5)
getvariogramfit(detail = c(details$smMean[[2]]), inwith = 1e-4, incutoff = 5)
getvariogramfit(detail = c(details$smMean[[3]]), inwith = 1e-4, incutoff = 5)



# detail and uncertainty maps
par(mfrow = c(3,2))
colfactors <- fields::tim.colors(n = 256)[cut(unlist(details$smMean[c(1:3)]), breaks = 256)]
pwcols <- c("blue", "grey", "red")[factor(c(details$hpout[[1]]$pw,
                                            details$hpout[[2]]$pw), levels = c(0, 0.5, 1))]

n <- length(y)
plot(locs, col = colfactors[1:n], pch = 20)
plot(locs, col = pwcols[1:n], pch = 20)
plot(locs, col = colfactors[(n+1):(2*n)], pch = 20)
plot(locs, col = pwcols[(n+1):(2*n)], pch = 20)
plot(locs, col = colfactors[(2*n+1):(3*n)], pch = 20)


# Roman Flury and Reinhard Furrer 2022
